import 'react-native-gesture-handler';
import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Login from '../src/screen/login';
import Register from '../src/screen/register';
import Tabs from './tabs';
import TopUpScreen from './screen/topUp';
import TopUpCompleteScreen from './screen/topUpComplete';
import Payment from './screen/payment';
import PaymentConfirmationScreen from './screen/paymentConfirmation';
import PaySuccesScreen from './screen/paySuccess';
import TransferScreen from './screen/transfer';
import TransferConfirm from './screen/transferConfirm';
import TransferSuccessScreen from './screen/transferSuccess';
import SplashScreen from './screen/splash';

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Splash"
        screenOptions={{headerShown: false}}>
        <Stack.Screen name="Splash" component={SplashScreen} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Account Registration" component={Register} />
        <Stack.Screen name="Home" component={Tabs} />
        <Stack.Screen name="Top Up" component={TopUpScreen} />
        <Stack.Screen name="Top Up Complete" component={TopUpCompleteScreen} />
        <Stack.Screen name="Payment" component={Payment} />
        <Stack.Screen name="Transfer" component={TransferScreen} />
        <Stack.Screen name="Transfer Confirm" component={TransferConfirm} />
        <Stack.Screen
          name="Transfer Success"
          component={TransferSuccessScreen}
        />
        <Stack.Screen
          name="Payment Confirmation"
          component={PaymentConfirmationScreen}
        />
        <Stack.Screen name="Payment Success" component={PaySuccesScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;

const styles = StyleSheet.create({});
