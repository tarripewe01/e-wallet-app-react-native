import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Feather from 'react-native-vector-icons/Feather';
import HomeScreen from '../screen/home';
import HistoryTransaction from '../screen/history';
import ProfileScreen from '../screen/profile';

const Tab = createBottomTabNavigator();

const Tabs = () => {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      tabBarOptions={{activeTintColor: '#4982C1'}}>
      <Tab.Screen
        name="Home"
        component={HomeScreen}
        options={{
          tabBarLabel: '',
          tabBarIcon: ({color, size}) => (
            <Feather name="home" color="#333333" size={20} />
          ),
        }}
      />
      <Tab.Screen
        name="History"
        component={HistoryTransaction}
        options={{
          tabBarLabel: '',
          tabBarIcon: ({color, size}) => (
            <Feather name="minimize-2" color="#333333" size={20} />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          tabBarLabel: '',
          tabBarIcon: ({color, size}) => (
            <Feather name="user" color="#333333" size={20} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default Tabs;

const styles = StyleSheet.create({});
