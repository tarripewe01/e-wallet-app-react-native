import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';

const Card = () => {
  return (
    <View style={styles.container}>
      <View style={styles.containerContent}>
        <Feather name="minimize-2" size={15} style={styles.icon} />
        <View>
            <Text style={styles.titleContent}>Rp. 80.000</Text>
            <Text style={styles.titleContent}>Transfer to 082240206861</Text>
        </View>
        <Text style={styles.titleContent}>20/08/2020</Text>
      </View>
    </View>
  );
};

export default Card;

const styles = StyleSheet.create({
  container: {
    width: 319,
    height: 72,
    backgroundColor: '#fff',
    marginBottom: 5,
    marginTop: 10,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.29,
    shadowRadius: 4.65,
    elevation: 7
  },
  containerContent: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    paddingTop: 15,
  },
  icon: {
      marginTop: 15,
      color: '#DADADA'
  },
  titleContent: {
    fontSize: 14,
    fontWeight: '300',
    lineHeight: 16.41,
    color: '#000',
  }
});
