import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

const PaymentConfirmationScreen = ({navigation}) => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity>
          <FontAwesome name="arrow-left" size={16} style={styles.icon} />
        </TouchableOpacity>
        <Text style={styles.headerText}>Payment Confirmation</Text>
      </View>

      <Image
        source={require('../../assets/images/payment.png')}
        style={styles.image}
      />

      <View style={styles.containerTitle}>
        <Text style={styles.title}>Rp. 60.000</Text>
      </View>

      <View style={styles.containerSubTitle}>
        <View style={styles.containerText}>
          <Text style={styles.Text1}>Payment To :</Text>
        </View>

        <View style={styles.containerText}>
          <Text style={styles.Text2}>Basicschool</Text>
        </View>

        <View style={styles.containerText}>
          <Text style={styles.Text3}>Jl. Ciparay No 20B, Kota Bandung</Text>
        </View>
      </View>

      <View style={styles.submitBtn}>
        <TouchableOpacity onPress={() => navigation.navigate('Payment Success')}>
          <Text style={styles.textBtn}>SUBMIT</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default PaymentConfirmationScreen;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
  },
  icon: {
    color: '#fff',
    paddingRight: 15,
    paddingLeft: 15,
    justifyContent: 'center',
    paddingTop: 15,
  },
  header: {
    width: 360,
    height: 56,
    backgroundColor: '#005690',
    flexDirection: 'row',
  },
  headerText: {
    color: '#fff',
    fontWeight: '500',
    fontSize: 18,
    lineHeight: 21.09,
    justifyContent: 'center',
    paddingTop: 15,
  },
  image: {
    alignSelf: 'center',
    marginTop: 96,
    width: 199,
    height: 199,
  },
  containerTitle: {
    alignSelf: 'center',
    width: 280,
    borderBottomColor: '#B2B2B2',
    borderBottomWidth: 1,
  },
  title: {
    fontWeight: '400',
    fontSize: 24,
    lineHeight: 28.13,
    alignSelf: 'center',
    paddingBottom: 10,
  },
  containerSubTitle: {
    backgroundColor: '#4982C1',
    width: 280,
    height: 129,
    borderRadius: 4,
    alignSelf: 'center',
    marginTop: 20,
  },
  containerText: {
    alignSelf: 'center',
  },
  Text1: {
    color: '#fff',
    fontWeight: '300',
    fontSize: 18,
    lineHeight: 21.09,
    paddingTop: 20,
  },
  Text2: {
    color: '#fff',
    fontWeight: '400',
    fontSize: 24,
    lineHeight: 28.13,
    paddingTop: 20,
  },
  Text3: {
    color: '#fff',
    fontWeight: '400',
    fontSize: 16,
    lineHeight: 18.75,
    paddingTop: 5,
  },
  submitBtn: {
    backgroundColor: '#4982C1',
    width: 280,
    height: 48,
    borderRadius: 4,
    alignSelf: 'center',
    marginTop: 30,
  },
  textBtn: {
    alignSelf: 'center',
    paddingVertical: 15,
    color: '#fff',
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 18.75,
  },
});
