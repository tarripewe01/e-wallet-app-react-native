import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button,
  StatusBar,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {color} from 'react-native-reanimated';

const Login = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#fff', flex: 1}}>
      <View>
        <View style={styles.round}>
          <Image
            source={require('../../assets/images/logo.png')}
            style={styles.logo}
          />
        </View>
      </View>
      <View style={{backgroundColor: '#fff', flex: 2}}>
        <Text style={styles.title}>e-wallet</Text>
        <View style={styles.form}>
          <TextInput placeholder="Email" style={styles.formInput} />
          <TextInput
            placeholder="Password"
            secureTextEntry={true}
            style={styles.formInput}
          />
          <TouchableOpacity style={styles.loginBtn} >
            <Text style={{color: '#fff'}} onPress={() => navigation.navigate('Home')}>LOGIN</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigation.navigate('Account Registration')}>
            <Text style={styles.registerBtn}>Registrasi</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  round: {
    backgroundColor: '#005690',
    width: 137,
    height: 137,
    borderRadius: 100,
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 50,
  },
  logo: {alignSelf: 'center'},
  title: {
    alignSelf: 'center',
    color: '#4982C1',
    fontWeight: '500',
    fontSize: 24,
    lineHeight: 28.13,
    marginTop: 10,
    marginBottom: 10,
  },
  form: {
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10,
  },
  formInput: {
    borderWidth: 1,
    borderColor: '#C3C3C3',
    marginBottom: 15,
    paddingLeft: 10,
  },
  loginBtn: {
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 16,
    lineHeight: 18.75,
    backgroundColor: '#4982C1',
    width: 320,
    height: 48,
    borderRadius: 4
  },
  registerBtn: {
    alignSelf: 'center',
    color: '#000',
    fontSize: 14,
    lineHeight: 28.13,
    marginTop: 20,
    marginBottom: 10,
  },
});
