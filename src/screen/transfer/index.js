import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  TextInput,
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

const TransferScreen = ({navigation}) => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity>
          <FontAwesome
            name="arrow-left"
            size={16}
            style={styles.icon}
            onPress={() => navigation.navigate('Home')}
          />
        </TouchableOpacity>
        <Text style={styles.headerText}>Transfer</Text>
      </View>

      <Image
        source={require('../../assets/images/transfer.png')}
        style={styles.image}
      />

      <View style={styles.containerInput}>
        <TextInput placeholder="Receiver Phone Number" style={styles.input} />
      </View>

      <View style={styles.containerBtn}>
        <TouchableOpacity
          onPress={() => navigation.navigate('Transfer Confirm')}>
          <Text style={styles.textBtn}>CHECK NUMBER</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default TransferScreen;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
  },
  icon: {
    color: '#fff',
    paddingRight: 15,
    paddingLeft: 15,
    justifyContent: 'center',
    paddingTop: 15,
  },
  header: {
    width: 360,
    height: 56,
    backgroundColor: '#005690',
    flexDirection: 'row',
  },
  headerText: {
    color: '#fff',
    fontWeight: '500',
    fontSize: 18,
    lineHeight: 21.09,
    justifyContent: 'center',
    paddingTop: 15,
  },
  image: {
    alignSelf: 'center',
    marginTop: 21,
    width: 240,
    height: 172,
  },
  containerInput: {
    alignSelf: 'center',
    marginTop: 70,
  },
  input: {
    borderWidth: 1,
    borderColor: '#C3C3C3',
    width: 280,
    height: 48,
    borderRadius: 4,
    paddingLeft: 15,
  },
  containerBtn: {
    backgroundColor: '#4982C1',
    width: 280,
    height: 48,
    borderRadius: 4,
    alignSelf: 'center',
    marginTop: 15,
  },
  textBtn: {
    color: '#fff',
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 18.75,
    alignSelf: 'center',
    paddingVertical: 15,
  },
});
