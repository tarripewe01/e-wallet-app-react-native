import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  TextInput,
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

const TransferConfirm = ({navigation}) => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity>
          <FontAwesome name="arrow-left" size={16} style={styles.icon} />
        </TouchableOpacity>
        <Text style={styles.headerText}>Transfer</Text>
      </View>

      <Image
        source={require('../../assets/images/transfer.png')}
        style={styles.image}
      />

      <View style={styles.containerInput}>
        <TextInput placeholder="Nominal Transfer" style={styles.input} />
      </View>

      <View style={styles.containerTitle}>
        <View>
          <Text style={styles.text}>Receiver :</Text>
        </View>

        <View>
          <Text style={styles.text2}>Dendy Aditya</Text>
        </View>
      </View>

      <View style={styles.containerBtn}>
        <TouchableOpacity onPress={() => navigation.navigate('Transfer Success')}>
          <Text style={styles.textBtn}>TRANSFER</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default TransferConfirm;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
  },
  icon: {
    color: '#fff',
    paddingRight: 15,
    paddingLeft: 15,
    justifyContent: 'center',
    paddingTop: 15,
  },
  header: {
    width: 360,
    height: 56,
    backgroundColor: '#005690',
    flexDirection: 'row',
  },
  headerText: {
    color: '#fff',
    fontWeight: '500',
    fontSize: 18,
    lineHeight: 21.09,
    justifyContent: 'center',
    paddingTop: 15,
  },
  image: {
    alignSelf: 'center',
    marginTop: 21,
    width: 240,
    height: 172,
  },
  containerInput: {
    alignSelf: 'center',
    marginTop: 70,
  },
  containerTitle: {
    alignSelf: 'center',
    marginTop: 20,
  },
  text: {
    fontWeight: '400',
    fontSize: 16,
    lineHeight: 18.75,
    paddingLeft: 10
  },
  text2: {
    fontWeight: '300',
    fontSize: 16,
    lineHeight: 18.75,
    paddingTop: 20,
    paddingBottom: 15
  },
  input: {
    borderWidth: 1,
    borderColor: '#C3C3C3',
    width: 280,
    height: 48,
    borderRadius: 4,
    paddingLeft: 15,
  },
  containerBtn: {
    backgroundColor: '#4982C1',
    width: 280,
    height: 48,
    borderRadius: 4,
    alignSelf: 'center',
    marginTop: 15,
  },
  textBtn: {
    color: '#fff',
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 18.75,
    alignSelf: 'center',
    paddingVertical: 15,
  },
});
