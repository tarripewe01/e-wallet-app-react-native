import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

const Payment = ({navigation}) => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity>
          <FontAwesome
            name="arrow-left"
            size={16}
            style={styles.icon}
            onPress={() => navigation.navigate('Home')}
          />
        </TouchableOpacity>
        <Text style={styles.headerText}>QR Payment</Text>
      </View>

      <View style={styles.containerQR}>
        <TouchableOpacity>
          <FontAwesome
            name="camera"
            size={24}
            style={styles.iconQR}
            onPress={() => navigation.navigate('Payment Confirmation')}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Payment;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
  },
  icon: {
    color: '#fff',
    paddingRight: 15,
    paddingLeft: 15,
    justifyContent: 'center',
    paddingTop: 15,
  },
  header: {
    width: 360,
    height: 56,
    backgroundColor: '#005690',
    flexDirection: 'row',
  },
  headerText: {
    color: '#fff',
    fontWeight: '500',
    fontSize: 18,
    lineHeight: 21.09,
    justifyContent: 'center',
    paddingTop: 15,
  },
  containerQR: {
    width: 319,
    height: 319,
    backgroundColor: '#C4C4C4',
    alignSelf: 'center',
    justifyContent: 'center',
    marginVertical: 160,
  },
  iconQR: {
    color: '#DADADA',
    alignSelf: 'center',
  },
});
