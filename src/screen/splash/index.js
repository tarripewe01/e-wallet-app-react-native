import React from 'react';
import { useEffect } from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';

const SplashScreen = ({navigation}) => {

    useEffect(() => {
        setTimeout(()=> {
            navigation.replace('Login');
        }, 3000)
    }, [navigation])
  return (
    <View style={styles.container}>
      <Image
        source={require('../../assets/images/bigLogo.png')}
        style={styles.logo}
      />

      <View style={styles.containerText}>
        <View>
          <Text style={styles.text1}>e-wallet apps</Text>
        </View>

        <View style={styles.containerText2}>
          <Text style={styles.text1}>Final Project</Text>
          <Text style={styles.text1}>React Native</Text>
        </View>
      </View>
    </View>
  );
};

export default SplashScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#005690',
  },
  logo: {
    alignSelf: 'center',
    marginTop: 100,
  },
  containerText: {
    alignSelf: 'center',
    marginTop: 50,
  },
  text1: {
    fontSize: 36,
    fontWeight: '300',
    lineHeight: 42.19,
    color: '#fff',
  },
  containerText2: {
    marginTop: 50,
  },
});
