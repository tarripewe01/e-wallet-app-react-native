import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  TextInput,
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

const TopUpScreen = ({navigation}) => {
  return (
    <View>
      <View style={styles.header}>
        <TouchableOpacity>
          <FontAwesome
            name="arrow-left"
            size={16}
            style={styles.icon}
            onPress={() => navigation.navigate('Home')}
          />
        </TouchableOpacity>
        <Text style={styles.headerText}>Top Up</Text>
      </View>

      <View>
        <Image
          source={require('../../assets/images/topup.png')}
          style={styles.image}
        />
      </View>

      <View style={styles.containerInput}>
        <TextInput placeholder="Nominal Top Up" style={styles.formInput} />
      </View>
      <View style={styles.containerBtn}>
        <TouchableOpacity
          style={styles.btnSubmit}
          onPress={() => navigation.navigate('Top Up Complete')}>
          <Text style={styles.textSubmit}>SUBMIT</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default TopUpScreen;

const styles = StyleSheet.create({
  icon: {
    color: '#fff',
    paddingRight: 15,
    paddingLeft: 15,
    justifyContent: 'center',
    paddingTop: 15,
  },
  header: {
    width: 360,
    height: 56,
    backgroundColor: '#005690',
    flexDirection: 'row',
  },
  headerText: {
    color: '#fff',
    fontWeight: '500',
    fontSize: 18,
    lineHeight: 21.09,
    justifyContent: 'center',
    paddingTop: 15,
  },
  image: {
    alignSelf: 'center',
    marginTop: 136,
    width: 156,
    height: 156,
  },
  containerInput: {
    alignSelf: 'center',
    width: 280,
    height: 48,
    backgroundColor: '#fff',
  },
  formInput: {
    borderWidth: 1,
    borderColor: '#C3C3C3',
    marginBottom: 15,
    paddingLeft: 10,
    width: 280,
    height: 48,
    fontWeight: '400',
    fontSize: 14,
    lineHeight: 16.41,
  },
  containerBtn: {
    alignSelf: 'center',
    width: 280,
    height: 48,
    marginTop: 15,
  },
  btnSubmit: {
    width: 280,
    height: 48,
    backgroundColor: '#4982C1',
    borderRadius: 4,
  },
  textSubmit: {
    alignSelf: 'center',
    paddingVertical: 15,
    color: '#fff',
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 18.75,
  },
});
