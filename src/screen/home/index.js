import React from 'react';
import {FlatList, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import Card from '../../component/card';

const HomeScreen = ({navigation}) => {
  return (
    <View>
      <View style={styles.header}>
        <Text style={styles.subtitle}>Your Balance</Text>
        <Text style={styles.title}>Rp. 1.234.567.000</Text>
      </View>

      <View style={styles.menu}>
        <View style={styles.containerMenu}>
          <View style={styles.menuIcon}>
            <TouchableOpacity onPress={() => navigation.navigate('Top Up')}>
              <Feather name="plus" size={23} style={styles.icon} />
            </TouchableOpacity>
            <Text style={styles.textIcon}>Top Up</Text>
          </View>

          <View style={styles.menuIcon}>
            <TouchableOpacity onPress={() => navigation.navigate('Payment')}>
              <Feather name="maximize" size={23} style={styles.icon} />
            </TouchableOpacity>
            <Text style={styles.textIcon}>QR Pay</Text>
          </View>
          <View style={styles.menuIcon}>
            <TouchableOpacity onPress={() => navigation.navigate('Transfer')}>
              <Feather name="arrow-right" size={23} style={styles.icon} />
            </TouchableOpacity >
            <Text style={styles.textIcon}>Transfer</Text>
          </View>
        </View>
      </View>

      <View style={styles.containerContent}>
        <Text style={styles.titleContent}>5 Latest Transaction</Text>
        <Card />
        <Card />
        <Card />
      </View>
    </View>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#fff',
    width: 360,
    height: 134,
    paddingLeft: 20,
    paddingBottom: 5,
    justifyContent: 'flex-end',
  },
  subtitle: {
    fontSize: 14,
    fontWeight: '400',
    lineHeight: 14.41,
    color: '#484848',
  },
  title: {
    fontSize: 36,
    fontWeight: '500',
    lineHeight: 42.19,
    color: '#575757',
    marginTop: 3,
  },
  menu: {
    height: 89,
    width: 320,
    backgroundColor: '#4982C1',
    alignSelf: 'center',
    marginTop: 20,
  },
  containerMenu: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 23,
    paddingTop: 15,
  },
  menuIcon: {
    width: 48,
    height: 48,
    backgroundColor: '#fff',
  },
  icon: {
    color: '#333333',
    alignSelf: 'center',
    justifyContent: 'center',
    paddingTop: 13,
  },
  textIcon: {
    color: '#fff',
    fontSize: 12,
    fontWeight: '500',
    lineHeight: 16.41,
    marginTop: 15,
    paddingLeft: 3,
  },
  containerContent: {
    paddingLeft: 20,
    marginTop: 30,
  },
  titleContent: {
    fontSize: 14,
    fontWeight: '300',
    lineHeight: 16.41,
    color: '#000',
  },
});
