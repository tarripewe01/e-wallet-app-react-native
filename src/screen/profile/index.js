import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';

const ProfileScreen = () => {
  return (
    <View>
      <View style={styles.container}>
        <View style={styles.round}></View>
        <View style={styles.containerText}>
          <Text style={styles.text}>Tarri Peritha Westi</Text>
          <View style={{paddingLeft: 15}}>
            <Text style={styles.text}>08126910xxxx</Text>
          </View>
        </View>
      </View>

      <View style={styles.containerBtn}>
        <TouchableOpacity style={styles.btn}>
          <Text style={styles.textBtn}>CHANGE PROFILE</Text>
        </TouchableOpacity>
      </View>

      <View style={styles.containerBtn}>
        <TouchableOpacity style={styles.btn}>
          <Text style={styles.textBtn}>CHANGE PASSWORD</Text>
        </TouchableOpacity>
      </View>

      <View style={styles.containerBtn}>
        <TouchableOpacity style={styles.btn}>
          <Text style={styles.textBtn}>LOGOUT</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default ProfileScreen;

const styles = StyleSheet.create({
  container: {
    width: 360,
    height: 224,
    backgroundColor: '#005690',
  },
  round: {
    width: 100,
    height: 100,
    backgroundColor: '#fff',
    borderRadius: 50,
    alignSelf: 'center',
    marginTop: 35,
  },
  containerText: {
    alignSelf: 'center',
    marginTop: 20,
  },
  text: {
    color: '#fff',
    fontSize: 18,
    fontWeight: '400',
    lineHeight: 21.09,
    marginBottom: 10,
  },
  containerBtn: {
    alignSelf: 'center',
    marginTop: 50,
    marginBottom: -30,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 12},
    shadowOpacity: 0.58,
    shadowRadius: 16.0,
    elevation: 24
  },
  btn: {
    width: 280,
    height: 48,
    backgroundColor: '#4982C1',
    borderRadius: 4,
  },
  textBtn: {
    alignSelf: 'center',
    paddingHorizontal: 4,
    paddingVertical: 15,
    color: '#fff',
  },
});
