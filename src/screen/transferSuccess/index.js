import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';

const TransferSuccessScreen = ({navigation}) => {
  return (
    <View style={styles.container}>
      <Image
        source={require('../../assets/images/transfer.png')}
        style={styles.image}
      />

      <View style={styles.containerTitle}>
        <Text style={styles.title}>Transfer Success!</Text>
        <Text style={styles.title}>Rp. 60.000</Text>
      </View>

      <View style={styles.containerSubTitle}>
        <View style={styles.containerText}>
          <Text style={styles.Text1}>20 August 2020</Text>
        </View>

        <View style={styles.containerText}>
          <Text style={styles.Text1}>Receiver : Dendy Aditya</Text>
        </View>

        <View style={styles.containerText}>
          <Text style={styles.Text1}>082240206xxx</Text>
        </View>
      </View>

      <View style={styles.submitBtn}>
        <TouchableOpacity
          onPress={() => navigation.navigate('Home')}>
          <Text style={styles.textBtn}>FINISH</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default TransferSuccessScreen;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
  },
  icon: {
    color: '#fff',
    paddingRight: 15,
    paddingLeft: 15,
    justifyContent: 'center',
    paddingTop: 15,
  },
  header: {
    width: 360,
    height: 56,
    backgroundColor: '#005690',
    flexDirection: 'row',
  },
  headerText: {
    color: '#fff',
    fontWeight: '500',
    fontSize: 18,
    lineHeight: 21.09,
    justifyContent: 'center',
    paddingTop: 15,
  },
  image: {
    alignSelf: 'center',
    marginTop: 96,
    width: 156,
    height: 156,
  },
  containerTitle: {
    alignSelf: 'center',
    width: 280,
  },
  title: {
    fontWeight: '400',
    fontSize: 24,
    lineHeight: 28.13,
    alignSelf: 'center',
    paddingBottom: 10,
    paddingTop: 25
  },
  containerSubTitle: {
    backgroundColor: '#4982C1',
    width: 280,
    height: 129,
    borderRadius: 4,
    alignSelf: 'center',
    marginTop: 20,
  },
  containerText: {
    alignSelf: 'center',
    marginTop: 15
  },
  Text1: {
    color: '#fff',
    fontWeight: '400',
    fontSize: 18,
    lineHeight: 21.09,
  },
  submitBtn: {
    backgroundColor: '#4982C1',
    width: 280,
    height: 48,
    borderRadius: 4,
    alignSelf: 'center',
    marginTop: 30,
  },
  textBtn: {
    alignSelf: 'center',
    paddingVertical: 15,
    color: '#fff',
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 18.75,
  },
});
