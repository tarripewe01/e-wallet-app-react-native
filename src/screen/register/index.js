import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

const Register = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#fff', flex: 1}}>
      <View style={styles.header}>
        <TouchableOpacity>
          <FontAwesome
            name="arrow-left"
            size={16}
            style={styles.icon}
            onPress={() => navigation.goBack('Login')}
          />
        </TouchableOpacity>
        <Text style={styles.headerText}>Account Registration</Text>
      </View>
      <View style={{backgroundColor: '#fff', marginTop: 151}}>
        <View style={styles.form}>
          <TextInput placeholder="Email" style={styles.formInput} />
          <TextInput
            placeholder="Password"
            secureTextEntry={true}
            style={styles.formInput}
          />
          <TextInput placeholder="Name" style={styles.formInput} />
          <TextInput placeholder="No Handphone" style={styles.formInput} />
          <TouchableOpacity style={styles.submitBtn}>
            <Text
              style={{color: '#fff'}}
              onPress={() => navigation.navigate('Home')}>
              SUBMIT
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default Register;

const styles = StyleSheet.create({
  icon: {
    color: '#fff',
    paddingRight: 15,
    paddingLeft: 15,
    justifyContent: 'center',
    paddingTop: 15,
  },
  header: {
    width: 360,
    height: 56,
    backgroundColor: '#005690',
    flexDirection: 'row',
  },
  headerText: {
    color: '#fff',
    fontWeight: '500',
    fontSize: 18,
    lineHeight: 21.09,
    justifyContent: 'center',
    paddingTop: 15,
  },
  form: {
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10,
  },
  formInput: {
    borderWidth: 1,
    borderColor: '#C3C3C3',
    marginBottom: 15,
    paddingLeft: 10,
  },
  submitBtn: {
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 16,
    lineHeight: 18.75,
    backgroundColor: '#4982C1',
    width: 320,
    height: 48,
    borderRadius: 4,
  },
});
