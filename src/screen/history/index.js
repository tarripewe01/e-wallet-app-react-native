import React from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import Card from '../../component/card';

const HistoryTransaction = () => {
  return (
    <ScrollView>
      <View style={styles.containerContent}>
        <Card />
        <Card />
        <Card />
        <Card />
        <Card />
        <Card />
        <Card />
        <Card />
        <Card />
        <Card />
        <Card />
      </View>
    </ScrollView>
  );
};

export default HistoryTransaction;

const styles = StyleSheet.create({
  containerContent: {
    paddingLeft: 20,
    marginTop: 30,
  },
});
