import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';

const TopUpCompleteScreen = ({navigation}) => {
  return (
    <View>
      <View>
        <Image
          source={require('../../assets/images/topup.png')}
          style={styles.image}
        />
      </View>

      <View style={styles.containerTitle}>
        <View>
          <Text style={styles.title}>Top Up Complete</Text>
        </View>

        <View style={styles.titlePrice}>
          <Text style={styles.title}>Rp. 60,000</Text>
        </View>
      </View>

      <View style={styles.containerSubTitle}>
        <Text style={styles.subTitle}>20 August 2020</Text>
        <Text style={styles.subTitle}>VA Mandiri</Text>
      </View>

      <View>
        <TouchableOpacity
          style={styles.containerBtn}
          onPress={() => navigation.navigate('Home')}>
          <Text style={styles.textBtn}>FINISH</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default TopUpCompleteScreen;

const styles = StyleSheet.create({
  image: {
    alignSelf: 'center',
    marginTop: 136,
    width: 156,
    height: 156,
  },
  containerTitle: {
    alignSelf: 'center',
  },
  title: {
    fontWeight: '400',
    fontSize: 24,
    lineHeight: 28.13,
  },
  titlePrice: {
    marginVertical: 15,
    marginHorizontal: 35,
  },
  containerSubTitle: {
    width: 280,
    height: 91,
    backgroundColor: '#4982C1',
    alignSelf: 'center',
  },
  subTitle: {
    fontWeight: '400',
    fontSize: 18,
    lineHeight: 21.09,
    color: '#fff',
    alignSelf: 'center',
    marginTop: 15,
  },
  containerBtn: {
    width: 280,
    height: 48,
    borderRadius: 4,
    backgroundColor: '#4982C1',
    alignSelf: 'center',
    marginTop: 30,
  },
  textBtn: {
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 18.75,
    color: '#fff',
    alignSelf: 'center',
    marginVertical: 15,
  },
});
